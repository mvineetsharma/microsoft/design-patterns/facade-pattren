﻿using System;
using System.Collections.Generic;

// Complex subsystems
class FruitInventory
{
    public Dictionary<string, int> AvailableFruits { get; set; }

    public FruitInventory()
    {
        AvailableFruits = new Dictionary<string, int>();
    }

    public void AddFruit(string fruitName, int quantity)
    {
        if (AvailableFruits.ContainsKey(fruitName))
        {
            AvailableFruits[fruitName] += quantity;
        }
        else
        {
            AvailableFruits[fruitName] = quantity;
        }
    }

    public int GetFruitCount(string fruitName)
    {
        if (AvailableFruits.ContainsKey(fruitName))
        {
            return AvailableFruits[fruitName];
        }
        return 0;
    }
}

class FruitPayment
{
    public void ProcessPayment(string fruitName, int quantity)
    {
        // Simulate a payment process
        Console.WriteLine($"Payment processed for {quantity} {fruitName}(s).");
    }
}

// Facade
class FruitFacade
{
    private FruitInventory inventory = new FruitInventory();
    private FruitPayment payment = new FruitPayment();

    public void AddFruitToInventory(string fruitName, int quantity)
    {
        inventory.AddFruit(fruitName, quantity);
    }

    public int GetAvailableFruitCount(string fruitName)
    {
        return inventory.GetFruitCount(fruitName);
    }

    public void BuyFruit(string fruitName, int quantity)
    {
        int availableQuantity = inventory.GetFruitCount(fruitName);
        if (availableQuantity >= quantity)
        {
            payment.ProcessPayment(fruitName, quantity);
            inventory.AddFruit(fruitName, -quantity); // Deduct purchased fruits from inventory
        }
        else
        {
            Console.WriteLine($"Not enough {fruitName} in stock.");
        }
    }
}

class Program
{
    static void Main()
    {
        FruitFacade fruitStore = new FruitFacade();

        fruitStore.AddFruitToInventory("Apple", 50);
        fruitStore.AddFruitToInventory("Banana", 30);

        Console.WriteLine("Inventory:");
        Console.WriteLine($"Apple: {fruitStore.GetAvailableFruitCount("Apple")}");
        Console.WriteLine($"Banana: {fruitStore.GetAvailableFruitCount("Banana")}");

        Console.WriteLine("\nBuying fruits:");
        fruitStore.BuyFruit("Apple", 10);
        fruitStore.BuyFruit("Banana", 20);
        fruitStore.BuyFruit("Cherry", 5);

        Console.WriteLine("\nInventory after purchases:");
        Console.WriteLine($"Apple: {fruitStore.GetAvailableFruitCount("Apple")}");
        Console.WriteLine($"Banana: {fruitStore.GetAvailableFruitCount("Banana")}");
    }
}
